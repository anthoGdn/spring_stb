/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.util;

import fr.univ.controller.HomeController;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class ValidatorXMLStb {
    
    public static boolean isValide(String xmlStb) throws SAXException, IOException, ParserConfigurationException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            factory.setValidating(false);
            factory.setNamespaceAware(true);
            
            
            URL url = HomeController.class.getProtectionDomain().getCodeSource().getLocation();  
            File file = new java.io.File(url.getFile());
            File parent = file.getParentFile();
            while (!"WEB-INF".equals(parent.getName())) {
                 parent = parent.getParentFile();
            }

            SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            factory.setSchema(schemaFactory.newSchema(new Source[] {
                new StreamSource(new File(parent.getAbsolutePath() + "/classes/stb.xsd"))
            }));

            DocumentBuilder builder = factory.newDocumentBuilder();
            SimpleErrorHandler error = new SimpleErrorHandler();
            builder.setErrorHandler(error);

            InputSource in = new InputSource(new StringReader(xmlStb));
            builder.parse(in);
  
            
            return !error.hasError();
                    
        } catch (ParserConfigurationException | SAXException | IOException e) {
        } 
        return false;
    }   
}
