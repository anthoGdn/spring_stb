package fr.univ.util;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class SimpleErrorHandler implements ErrorHandler  {
    
// ATTRIBUTES
    private boolean errorOccured;
    
// CONSTRUCTORS
    public SimpleErrorHandler() {
        super();
        errorOccured = false;
    }
    
// METHODES
    public boolean hasError() {
        return errorOccured;
    }
            
            
    public void warning(SAXParseException e) throws SAXException {
        errorOccured = true;
        System.out.println(e.getMessage());
    }

    public void error(SAXParseException e) throws SAXException {
        errorOccured = true;
        System.out.println(e.getMessage());
    }

    public void fatalError(SAXParseException e) throws SAXException {
        errorOccured = true;
        System.out.println(e.getMessage());
    }
    
}
