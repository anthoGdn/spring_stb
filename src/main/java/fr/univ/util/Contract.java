package fr.univ.util;

/**
 * Class to factorize the usual test of the arguments
 */
public class Contract {
    private Contract() {
        // Singleton
    }
    
    public static void check(boolean test, String message) {
        if (!test) {
            throw new IllegalArgumentException(message + " must not be null.");
        }
    }
    
    public static void checkArgumentNotNull(Object test, String message) {
        if (test == null) {
            throw new IllegalArgumentException(message + " must not be null.");
        }
    }

    public static void checkOrFailure(boolean test, String message) {
        if (!test) {
            throw new AssertionError(message);
        }
    }
    public static void checkOrFailure(boolean test) {
        checkOrFailure(test, "Empty Message");
    }
}
