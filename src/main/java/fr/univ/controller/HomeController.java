/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.controller;

import fr.univ.entities.Stb;
import fr.univ.entities.StbResume;
import fr.univ.entities.StbResumeList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import javax.servlet.http.HttpServletRequest;
 
@Controller
public class HomeController {
// ATTRIBUTES
    private static final String URL = "http://stb-spring-ag-yh.herokuapp.com";
    private static final String INDEX = URL;
    private static final String RESUME = URL + "/resume";
    private static final String RESUME_STB = URL + "/resume/";
    private static final String DEPOT = URL + "/depot";
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(ModelMap model) {    
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(INDEX, String.class);  
        model.addAttribute("message", result);
        return "index";
   }
 
    @RequestMapping(value = "/resume", method = RequestMethod.GET)
    public String resume(ModelMap model) {
        RestTemplate restTemplate = new RestTemplate();
        StbResumeList result = restTemplate.getForObject(RESUME, StbResumeList.class);
        List<StbResume> resumeList = result.getStb();
        model.addAttribute("resumeList", resumeList);
        return "resume";
    }
   
    
    @RequestMapping(value = "/resume/" + "{id}")
    public String getStbById (ModelMap model, @PathVariable("id") Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        Stb stb = restTemplate.getForObject(RESUME_STB + id, Stb.class);
        model.addAttribute("stb", stb);
        return "resumeStb";
    }
    
   
   @RequestMapping(value = "/depot", method = RequestMethod.GET)
   public String depot() {
      return "depot";
   }
   
   @RequestMapping(value = "/addStb", method = RequestMethod.POST)
   public String addStb(HttpServletRequest request, ModelMap model) {
        String stb = request.getParameter("stb");
        RestTemplate restTemplate = new RestTemplate();
        Stb result = restTemplate.postForObject(DEPOT, stb, Stb.class);
        if (result != null) {
            model.addAttribute("added", true);
            model.addAttribute("title", result.getTitle());
            model.addAttribute("version", result.getVersion());
            model.addAttribute("date", result.getDate());
            model.addAttribute("description", result.getDescription());
            model.addAttribute("id", result.getId());
        } else {
            model.addAttribute("added", false);
        }
      return "addStb";
   }
}
