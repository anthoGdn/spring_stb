/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "functionalities")
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "functionalities")
public class Functionalities {
    @XmlElement
    private List<Functionality> functionality;

    @Column(name = "Functionalities", nullable = false)
    public List<Functionality> getFunctionality() {
        return functionality;
    }

    public void setFunctionality(List<Functionality> functionality) {
        this.functionality = functionality;
    }
}
