/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.entities;

import fr.univ.util.Contract;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement (name = "stb")
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "stb")
public class Stb implements Serializable {
    
// ATTRIBUTES
    @XmlElement
    private Integer id;
    @XmlElement
    private String title;
    @XmlElement
    private String version;
    @XmlElement
    private String date;
    @XmlElement
    private String description;
    @XmlElement
    private Client client;
    @XmlElement
    private Team team;
    @XmlElement
    private Functionalities functionalities;
    @XmlElement
    private String commentaire;
    
// REQUESTS  
    @Column(name = "StbDate", nullable = false)
    public String getDate() {
        return date;
    }
    
    @Column(name = "StbDescription", nullable = false)
    public String getDescription() {
        return description;
    }
    
    @Id
    @Column(name = "StbId")
    public Integer getId() {
        return id;
    }
    
    @Column(name = "StbTitle", nullable = false)
    public String getTitle() {
        return title;
    }
    
    @Column(name = "StbVersion", nullable = false)
    public String getVersion() {
        return version;
    }
    
    @Column(name = "StbClient")
    public Client getClient() {
        return client;
    }
    
    @Column(name = "StbTeam")
    public Team getTeam() {
        return team;
    }
    
    @Column(name = "StbFunctionalities")
    public Functionalities getFunctionalities() {
        return functionalities;
    }
    
    @Column(name = "StbComentaire")
    public String getCommentaire() {
        return commentaire;
    }
    
    @Override
    public String toString() {
        return id + ": title = " + title + ", date = " + date + ", version = " + version;
    }
    
// COMMANDES
    public void setDate(String date) {
        Contract.checkArgumentNotNull(date, "date");
        this.date = date;
    }

    public void setDescription(String description) {
        Contract.checkArgumentNotNull(description, "description");
        this.description = description;
    }
    
    public void setId(Integer id) {
        Contract.checkArgumentNotNull(id, "id");
        this.id = id;
    }
    
    public void setTitle(String title) {
        Contract.checkArgumentNotNull(title, "title");
        this.title = title;
    }

    public void setVersion(String version) {
        Contract.checkArgumentNotNull(version, "version");
        this.version = version;
    } 
    
    public void setClient(Client client) {
        Contract.checkArgumentNotNull(client, "client");
        this.client = client;
    } 
    
    public void setTeam(Team team) {
        Contract.checkArgumentNotNull(team, "team");
        this.team = team;
    } 
    
    public void setFunctionalities(Functionalities functionalities) {
        Contract.checkArgumentNotNull(functionalities, "functionalities");
        this.functionalities = functionalities;
    } 
    
    public void setCommentaire(String commentaire) {
        Contract.checkArgumentNotNull(commentaire, "commentaire");
        this.commentaire = commentaire;
    }  
}
