/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.entities;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name = "functionality")
@XmlAccessorType(XmlAccessType.NONE)
@Table(name = "functionality")
@XmlType(propOrder = {"description", "priority", "requirements"})
public class Functionality {
    @XmlElement
    private String description;
    @XmlAttribute
    private String priority;
    @XmlElement
    private Requirements requirements;

    @Column(name = "FunctionalityDescription", nullable = false)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "FunctionalityPriority", nullable = false)
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Column(name = "FunctionalityRequirements", nullable = false)
    public Requirements getRequirements() {
        return requirements;
    }

    public void setRequirements(Requirements requirements) {
        this.requirements = requirements;
    }
    
    
}
