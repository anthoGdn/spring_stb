/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.entities;

import fr.univ.util.Contract;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "stb")
@XmlAccessorType(XmlAccessType.NONE)
public class StbResume {
    @XmlElement
    private Integer id;
    @XmlElement
    private String title;
    @XmlElement
    private String version;
    @XmlElement
    private String date;
    @XmlElement
    private String description; 
    
    public String getDate() {
        return date;
    }
    
    public String getDescription() {
        return description;
    }
    
    @Id
    public Integer getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getVersion() {
        return version;
    }
    
    @Override
    public String toString() {
        return id + ": title = " + title + ", date = " + date + ", version = " + version;
    }
    
// COMMANDES
    public void setDate(String date) {
        Contract.checkArgumentNotNull(date, "date");
        this.date = date;
    }

    public void setDescription(String description) {
        Contract.checkArgumentNotNull(description, "description");
        this.description = description;
    }
    
    public void setId(Integer id) {
        Contract.checkArgumentNotNull(id, "id");
        this.id = id;
    }
    
    public void setTitle(String title) {
        Contract.checkArgumentNotNull(title, "title");
        this.title = title;
    }

    public void setVersion(String version) {
        Contract.checkArgumentNotNull(version, "version");
        this.version = version;
    } 
}
