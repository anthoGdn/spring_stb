/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univ.entities;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "resume")
@XmlAccessorType(XmlAccessType.NONE)
public class StbResumeList {
    @XmlElement
    private List<StbResume> stb;

    public List<StbResume> getStb() {
        return stb;
    }

    public void setStb(List<StbResume> stbs) {
        this.stb = stbs;
    }
    
    
}
