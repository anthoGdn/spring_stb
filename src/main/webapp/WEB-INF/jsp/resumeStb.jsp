<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="fr.univ.entities.Stb" %>
<%@ page import="fr.univ.entities.Team" %>
<%@ page import="fr.univ.entities.Person" %>
<%@ page import="fr.univ.entities.Functionalities" %>
<%@ page import="fr.univ.entities.Functionality" %>
<%@ page import="fr.univ.entities.Requirements" %>
<%@ page import="fr.univ.entities.Requirement" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>Stb - Resume Stb</title>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"  type="text/css" />
        <link href="<c:url value="/resources/css/layout.css"/>" rel="stylesheet"  type="text/css">
        <link href="<c:url value="/resources/css/resumeStb.css"/>" rel="stylesheet"  type="text/css">
        <!-- Custom Fonts -->
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <!--<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>    
    <body id="page-top">
        <% Stb stb = (Stb)request.getAttribute("stb");
        List<Person> persons = stb.getTeam().getPerson();
        List<Functionality> functionalities = stb.getFunctionalities().getFunctionality();
        int i = 0, k = 0;
        %>
        <c:import url="/WEB-INF/jsp/import/navbar.jsp"/>
        <div id="index" class="body">
            <h1> STB n� <%= stb.getId() %> : <%= stb.getTitle() %> </h1>
            <table class="table">
                <caption>
                    Description
                </caption>
                <tr>
                    <td>
                        Titre :
                    </td>
                    <td>
                        <%= stb.getTitle() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Version : 
                    </td>
                    <td>
                        <%= stb.getVersion() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date : 
                    </td>
                    <td>
                        <%= stb.getDate() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description : 
                    </td>
                    <td>
                        <%= stb.getDescription() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Commentaire : 
                    </td>
                    <td>
                        <%= stb.getCommentaire() %>
                    </td>
                </tr>
            </table>
            <table class="table">
                <caption>    
                    Information sur le client
                </caption>
                <tr>
                    <td>
                        Entit� : 
                    </td>
                    <td>
                        <%= stb.getClient().getEntity() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contact : 
                    </td>
                    <td>
                        <%= stb.getClient().getContact() %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Code : 
                    </td>
                    <td>
                        <%= stb.getClient().getCode() %>
                    </td>
                </tr>
            </table>
            <table class="table">
                <caption>
                    L'�quipe
                </caption>
                <% for (i = 0; i < persons.size(); i++) { %>
                    <tr>
                        <td class="caseMember">
                            Membre n�<%= i + 1 %> :
                        </td>
                        <td>
                            <% if (persons.get(i).getGender().equals("homme")) { %>
                                M.
                            <% } else { %>
                                Mme.
                            <% } %>
                            <%= persons.get(i).getNom() %> <%= persons.get(i).getPrenom() %>
                        </td>
                    </tr>
                <% } %>
                <% i = 0; %>
            </table>
            <% for (i = 0; i < functionalities.size(); i++) { %>
                <table class="table table-bordered">
                    <caption>Fonctionalit� <%= i + 1 %></caption>
                    <tr>
                        <td>
                            Priorit� :
                        </td>
                        <td>
                            <%= functionalities.get(i).getPriority() %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description :
                        </td>
                        <td>
                            <%= functionalities.get(i).getDescription() %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Exigences fonctionnelles :
                        </td>
                        <td>
                            <ul class="list-unstyled">
                            <% for (k = 0; k < functionalities.get(i).getRequirements().getRequirement().size(); k++) { 
                                Requirement requirement = functionalities.get(i).getRequirements().getRequirement().get(k);
                            %>
                                <li> Exigence <%= requirement.getId() %> (Priorit� <%= requirement.getPriority() %>) : <%= requirement.getDescription() %></li>
                            <% } %>
                          </ul>
                        </td>
                    </tr>
                </table>
            <% } %>
        </div>

        <c:import url="/WEB-INF/jsp/import/footer.jsp"/>
    </body>

</html>
