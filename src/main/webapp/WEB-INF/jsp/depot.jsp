<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>Stb - Depot</title>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"  type="text/css" />
        <link href="<c:url value="/resources/css/layout.css"/>" rel="stylesheet"  type="text/css">
        <link href="<c:url value="/resources/css/depot.css"/>" rel="stylesheet"  type="text/css">
        <!-- Custom Fonts -->
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <!--<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>    
    <body id="page-top">
        <c:import url="/WEB-INF/jsp/import/navbar.jsp"/>
        <div id="index" class="body">
            <h2>D�poser une stb</h2>
            <form method="POST" action="addStb.html">
            <textarea name="stb">

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<stb>
	<title> ... </title>
	<version> ... </version>
	<date> ... </date>
	<description> ... </description>
	<client>
		<entity> ... </entity>
		<contact> ... </contact>
		<code> un nombre </code>
	</client>
	<team>
		<person gender="homme ou femme">
			<nom> ... </nom>
			<prenom> ... </prenom>
		</person>
		<person gender="homme ou femme">
			<nom> ... </nom>
			<prenom> ... </prenom>
		</person>
	</team>
	<functionalities>
		<functionality priority=" Compris entre 1 et 10 ">
			<description> ... </description>
			<requirements>
				<requirement>
					<id> ... </id>
					<description> ... </description>
					<priority> Compris entre 1 et 10 </priority>
				</requirement>
				<requirement>
					<id> ... </id>
                                        <description> ... </description>
					<priority> Compris entre 1 et 10 </priority>
				</requirement>
			</requirements>
		</functionality>
		<functionality priority=" Compris entre 1 et 10 ">
			<description> ... </description>
			<requirements>
				<requirement>
					<id> ... </id>
					<description> ... </description>
					<priority> Compris entre 1 et 10 </priority>
				</requirement>
					<requirement>
					<id> ... </id>
					<description> ... </description>
					<priority> Compris entre 1 et 10 </priority>
				</requirement>
			</requirements>
		</functionality>
	</functionalities>
	<commentaire> Balises non obligatoire </commentaire>
</stb>
                </textarea>
                <input type="submit" class="btn btn-success" value="Envoyer"/>
            <form>
        </div>
        <c:import url="/WEB-INF/jsp/import/footer.jsp"/>
    </body>

</html>
