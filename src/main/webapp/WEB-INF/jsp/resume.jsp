<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="fr.univ.entities.StbResume" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>Stb - Resume</title>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"  type="text/css" />
        <link href="<c:url value="/resources/css/layout.css"/>" rel="stylesheet"  type="text/css">
        <link href="<c:url value="/resources/css/resume.css"/>" rel="stylesheet"  type="text/css">
        <!-- Custom Fonts -->
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <!--<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>    
    <body id="page-top">
        <% ArrayList<StbResume> resumeList = (ArrayList<StbResume>)request.getAttribute("resumeList");
        int i;
        %>
        <c:import url="/WEB-INF/jsp/import/navbar.jsp"/>
        <div id="index" class="body">
            <h2>La liste des stb</h2>
            <div class="container-fluid">
                <% if (resumeList != null) { 
                    for (i = 0; i < resumeList.size(); i++) { %>
                        <table class="table table-hover">
                            <caption>
                                <div class="rowTitle">
                                    <a href="resume/<%= resumeList.get(i).getId() %>.html" >
                                        Sp�cification techniques des besoins n�<%= resumeList.get(i).getId() %>
                                    </a>
                                </div>
                            </caption>
                            <tr>
                                <td>
                                    Titre :  
                                </td>
                                <td>
                                    <%= resumeList.get(i).getTitle() %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Version :  
                                </td>
                                <td>
                                    <%= resumeList.get(i).getVersion() %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Date :  
                                </td>
                                <td>
                                    <%= resumeList.get(i).getDate() %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description :  
                                </td>
                                <td>
                                    <%= resumeList.get(i).getDescription() %>
                                </td>
                            </tr>
                        </table>
                <%  }
                }%>
            </div>
        </div>

        <c:import url="/WEB-INF/jsp/import/footer.jsp"/>
    </body>

</html>
