<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>Stb - Ajout</title>
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"  type="text/css" />
        <link href="<c:url value="/resources/css/layout.css"/>" rel="stylesheet"  type="text/css">
        <link href="<c:url value="/resources/css/addStb.css"/>" rel="stylesheet"  type="text/css">
        <!-- Custom Fonts -->
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <!--<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>    
    <body id="page-top">
        <% boolean added = (boolean) request.getAttribute("added"); %>
        
        
        <c:import url="/WEB-INF/jsp/import/navbar.jsp"/>
        <header>
            <div id="index" class="body">
                <% if (added) { %>
                    <h2>La Stb a �t� ajout�e.</h2>
                    <table class="table">
                        <caption>
                            <a href="resume/${id}.html" >
                            Sp�cification techniques des besoins n�${id}
                            </a>
                        </caption>
                        <tr>
                            <td>Title</td>
                            <td>${title}</td>
                        </tr>
                        <tr>
                            <td>Version</td>
                            <td>${version}</td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td>${date}</td>
                        </tr>
                         <tr>
                            <td>Description</td>
                            <td>${description}</td>
                        </tr>
                    </table>  
                <% } else { %>
                    <h2>La Stb est invalide, elle n'a donc pas pu �tre ajout�e</h2>
                <% } %>
            </div>
        </header>

        <c:import url="/WEB-INF/jsp/import/footer.jsp"/>
    </body>

</html>
